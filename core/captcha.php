<?php
/**
 * captcha.php  # generates captcha code 
 *
 * @package Blackcat Network
 * @author nitestryker
 * @copyright 2014 nitestryker software
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @version 1.0 Beta
 */
 
// check if session is already started PHP > 5.4.0
if(session_id() == '') {
    session_start();
}

// check if the cid has been generate already 
if (isset($_GET['cid']))
  {
$code = $_GET['cid'];
$im = imagecreatetruecolor(50, 24);
$bg = imagecolorallocate($im, 22, 86, 165); //background color blue
$fg = imagecolorallocate($im, 255, 255, 255);//text color white
imagefill($im, 0, 0, $bg);
imagestring($im, 5, 5, 5,  $code, $fg);
header("Cache-Control: no-cache, must-revalidate");
header('Content-type: image/png');
imagepng($im);
imagedestroy($im);
  } else {
 
 // if not generate new one 
$code=rand(1000,9999);
$_SESSION["code"]=$code;
$_SESSION['captcha']=$code;
$im = imagecreatetruecolor(50, 24);
$bg = imagecolorallocate($im, 22, 86, 165); //background color blue
$fg = imagecolorallocate($im, 255, 255, 255);//text color white
imagefill($im, 0, 0, $bg);
imagestring($im, 5, 5, 5,  $code, $fg);
header("Cache-Control: no-cache, must-revalidate");
header('Content-type: image/png');
imagepng($im);
imagedestroy($im);
}
?>
<?php
/**
 * main.tpl.php (the sites main template)
 *
 * @package Blackcat Network
 * @author nitestryker
 * @copyright 2014 nitestryker software
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @version 1.0 Beta
 */

 ?>
 <!DOCTYPE html>

<html>

<head>

<title>blackcat Network</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="lib/css/style.css" />

<script src="lib/js/jquery-2.1.1.min.js"></script>

<script src="lib/js/api.min.js"></script>

<script src="lib/js/jquery.fancybox.pack.js"></script>

<script src="lib/js/openpgp.min.js"></script>

<script type="text/javascript" src="lib/js/jquery.validate.js" /></script>
<script type="text/javascript" src="lib/js/jquery-validate.bootstrap-tooltip.js" /></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/default.js"></script>
<!----ajax to verify Captacha code & form validation -->
<script type="text/javascript">
	$(function()
{
 
    //jQuery Captcha Validation
    WEBAPP = {
 
        settings: {},
        cache: {},
 
        init: function() {
 
            //DOM cache
            this.cache.$form = $('#login_form');
            this.cache.$refreshCaptcha = $('#refresh-captcha');
            this.cache.$captchaImg = $('img#captcha');
            this.cache.$captchaInput = $(':input[name="captcha"]');
 
            this.eventHandlers();
            this.setupValidation();
 
        },
 
        eventHandlers: function() {
 
            //generate new captcha
            WEBAPP.cache.$refreshCaptcha.on('click', function(e)
            {
                WEBAPP.cache.$captchaImg.attr("src","core/captcha.php");
            });
        },
 
        setupValidation: function()
        {
 
            WEBAPP.cache.$form.validate({
               onkeyup: false,
               rules: {
                    "username": {
                        "required": true
                    },
                    "password": {
                        "required": true
                    },
                    "captcha": {
                        "required": true,
                        "remote" :
                        {
                          url: 'core/validate.php',
                          type: "post",
                          data:
                          {
                              code: function()
                              {
                                  return WEBAPP.cache.$captchaInput.val();
                              }
                          }
                        }
                    }
                },
                messages: {
                    "username": "please enter your username",
                    "password": "Please enter your password.",
                    "captcha": {
                        "required": "Please enter the verifcation code. -->",
                        "remote": "Verication code incorrect, please try again."
                    }
                },
                submitHandler: function(form)
                {
                    /* -------- AJAX SUBMIT ----------------------------------------------------- */
 
                    var submitRequest = $.ajax({
                         type: "POST",
                         url: "next.php",
                         data: {
                            "data": WEBAPP.cache.$form.serialize()
                        }
                    });
 
                    submitRequest.done(function(msg)
                    {
                        //success
                        console.log('success');
                        $('body').html('<h1>captcha correct, submit form success!</h1>');
                    });
 
                    submitRequest.fail(function(jqXHR, textStatus)
                    {
                        //fail
                        console.log( "fail - an error occurred: (" + textStatus + ")." );
                    });
 
                }
 
            });
 
        }
 
    }
 
    WEBAPP.init();
 
});
	</script>
</head>
<body>


	<div class="main-bg"></div>

	<div class="main-logo">BlackCat<span>Network</span></div>

	<div class="auth-box">

		<div class="auth-box-inner">

			<a class="to-register" href="register">Need an account?</a>

			<form id="login_form" action="#" method="post">

                <input type='hidden' name='csrfmiddlewaretoken' value='5h2SF7M6SFy3sa6qBMIEEQYbA2i69tuE' />

				<div class="form-groups">

					<input name="username" type="text" placeholder="Username" id="username" onblur="uname()"/>

				</div>

				<div class="form-groups">

					<input name="password" type="password" placeholder="Password" />

				</div>

				<div class="form-groups">
                    <label class="" for="captcha"></label>
                      <div id="captcha-wrap">                
					<input name="captcha" type="text" placeholder="Captcha" />
					<img src="core/captcha.php" id="captcha" Alt="Captcha Code" placeholder="Captcha"/>
				</div>
                <div class="form-groups">
                
					<label>&nbsp;</label>

					<input type="submit" value="Login"/>

				</div>

			</form>

		</div>

	</div>

</body>



</html>
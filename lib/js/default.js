// Input 0
"undefined" != typeof YUI && (YUI._YUI = YUI);
var YUI = function() {
  var a = 0, b = this, c = arguments, d = c.length, e = function(a, b) {
    return a && a.hasOwnProperty && a instanceof b;
  }, f = "undefined" !== typeof YUI_config && YUI_config;
  e(b, YUI) ? (b._init(), YUI.GlobalConfig && b.applyConfig(YUI.GlobalConfig), f && b.applyConfig(f), d || b._setup()) : b = new YUI;
  if (d) {
    for (;a < d;a++) {
      b.applyConfig(c[a]);
    }
    b._setup();
  }
  b.instanceOf = e;
  return b;
};
(function() {
  var a, b, c = "3.3.0", d = function() {
  }, e = Array.prototype.slice, f = {"io.xdrReady":1, "io.xdrResponse":1, "SWF.eventHandler":1}, g = "undefined" != typeof window, h = g ? window : null, m = g ? h.document : null, l = (a = m && m.documentElement) && a.className, r = {}, p = (new Date).getTime(), s = function(a, b, c, d) {
    a && a.addEventListener ? a.addEventListener(b, c, d) : a && a.attachEvent && a.attachEvent("on" + b, c);
  }, t = function(a, b, c, d) {
    if (a && a.removeEventListener) {
      try {
        a.removeEventListener(b, c, d);
      } catch (k) {
      }
    } else {
      a && a.detachEvent && a.detachEvent("on" + b, c);
    }
  }, k = function() {
    YUI.Env.windowLoaded = !0;
    YUI.Env.DOMReady = !0;
    g && t(window, "load", k);
  }, u = function(a, b) {
    var c = a.Env._loader;
    c ? (c.ignoreRegistered = !1, c.onEnd = null, c.data = null, c.required = [], c.loadType = null) : (c = new a.Loader(a.config), a.Env._loader = c);
    return c;
  }, n = function(a, b) {
    for (var c in b) {
      b.hasOwnProperty(c) && (a[c] = b[c]);
    }
  }, w = {success:!0};
  a && -1 == l.indexOf("yui3-js-enabled") && (l && (l += " "), a.className = l + "yui3-js-enabled");
  -1 < c.indexOf("@") && (c = "3.2.0");
  a = {applyConfig:function(a) {
    a = a || d;
    var b, c, k = this.config, e = k.modules, u = k.groups, g = k.rls, f = this.Env._loader;
    for (c in a) {
      a.hasOwnProperty(c) && (b = a[c], e && "modules" == c ? n(e, b) : u && "groups" == c ? n(u, b) : g && "rls" == c ? n(g, b) : "win" == c ? (k[c] = b.contentWindow || b, k.doc = k[c].document) : "_yuid" != c && (k[c] = b));
    }
    f && f._config(a);
  }, _config:function(a) {
    this.applyConfig(a);
  }, _init:function() {
    var a, b = YUI.Env, d = this.Env, k;
    this.version = c;
    if (!d) {
      d = this.Env = {mods:{}, versions:{}, base:"http://yui.yahooapis.com/", cdn:"http://yui.yahooapis.com/" + c + "/build/", _idx:0, _used:{}, _attached:{}, _yidx:0, _uidx:0, _guidp:"y", _loaded:{}, serviced:{}, getBase:b && b.getBase || function(b, c) {
        var k, e, y, n, q;
        e = m && m.getElementsByTagName("script") || [];
        for (y = 0;y < e.length;y += 1) {
          if (n = e[y].src) {
            if (k = (q = n.match(b)) && q[1]) {
              if (a = q[2]) {
                q = a.indexOf("js"), -1 < q && (a = a.substr(0, q));
              }
              (q = n.match(c)) && q[3] && (k = q[1] + q[3]);
              break;
            }
          }
        }
        return k || d.cdn;
      }};
      d._loaded[c] = {};
      if (b && this !== YUI) {
        d._yidx = ++b._yidx, d._guidp = ("yui_" + c + "_" + d._yidx + "_" + p).replace(/\./g, "_");
      } else {
        if (YUI._YUI) {
          b = YUI._YUI.Env;
          d._yidx += b._yidx;
          d._uidx += b._uidx;
          for (k in b) {
            k in d || (d[k] = b[k]);
          }
          delete YUI._YUI;
        }
      }
      this.id = this.stamp(this);
      r[this.id] = this;
    }
    this.constructor = YUI;
    this.config = this.config || {win:h, doc:m, debug:!0, useBrowserConsole:!0, throwFail:!0, bootstrap:!0, cacheUse:!0, fetchCSS:!0};
    this.config.base = YUI.config.base || this.Env.getBase(/^(.*)yui\/yui([\.\-].*)js(\?.*)?$/, /^(.*\?)(.*\&)(.*)yui\/yui[\.\-].*js(\?.*)?$/);
    a && "-min.-debug.".indexOf(a) || (a = "-min.");
    this.config.loaderPath = YUI.config.loaderPath || "loader/loader" + (a || "-min.") + "js";
  }, _setup:function(a) {
    var b = [], c = YUI.Env.mods, d = this.config.core || "get rls intl-base loader yui-log yui-later yui-throttle".split(" ");
    for (a = 0;a < d.length;a++) {
      c[d[a]] && b.push(d[a]);
    }
    this._attach(["yui-base"]);
    this._attach(b);
  }, applyTo:function(a, b, c) {
    if (!(b in f)) {
      return this.log(b + ": applyTo not allowed", "warn", "yui"), null;
    }
    a = r[a];
    var d, k, e;
    if (a) {
      d = b.split(".");
      k = a;
      for (e = 0;e < d.length;e += 1) {
        (k = k[d[e]]) || this.log("applyTo not found: " + b, "warn", "yui");
      }
      return k.apply(a, c);
    }
    return null;
  }, add:function(a, b, c, d) {
    d = d || {};
    var k = YUI.Env;
    b = {name:a, fn:b, version:c, details:d};
    var e, n = k.versions;
    k.mods[a] = b;
    n[c] = n[c] || {};
    n[c][a] = b;
    for (e in r) {
      r.hasOwnProperty(e) && (c = r[e].Env._loader) && (c.moduleInfo[a] || c.addModule(d, a));
    }
    return this;
  }, _attach:function(a, b) {
    var c, d, k, e, n, u, g = YUI.Env.mods, f, h = this.Env._attached, w = a.length;
    for (c = 0;c < w;c++) {
      if (!h[a[c]]) {
        if (d = a[c], k = g[d]) {
          h[d] = !0;
          e = k.details;
          n = e.requires;
          u = e.use;
          e = e.after;
          if (n) {
            for (f = 0;f < n.length;f++) {
              if (!h[n[f]]) {
                if (!this._attach(n)) {
                  return!1;
                }
                break;
              }
            }
          }
          if (e) {
            for (f = 0;f < e.length;f++) {
              if (!h[e[f]]) {
                if (!this._attach(e)) {
                  return!1;
                }
                break;
              }
            }
          }
          if (u) {
            for (f = 0;f < u.length;f++) {
              if (!h[u[f]]) {
                if (!this._attach(u)) {
                  return!1;
                }
                break;
              }
            }
          }
          if (k.fn) {
            try {
              k.fn(this, d);
            } catch (m) {
              return this.error("Attach error: " + d, m, d), !1;
            }
          }
        } else {
          (k = this.Env._loader) && k.moduleInfo[d] || this.message("NOT loaded: " + d, "warn", "yui");
        }
      }
    }
    return!0;
  }, use:function() {
    var a = e.call(arguments, 0), b = a[a.length - 1], c;
    this.Lang.isFunction(b) ? a.pop() : b = null;
    this._loading ? (this._useQueue = this._useQueue || new this.Queue, this._useQueue.add([a, b])) : (c = a.join(), this.config.cacheUse && this.Env.serviced[c] ? this._notify(b, w, a) : this._use(a, function(d, k) {
      d.config.cacheUse && (d.Env.serviced[c] = !0);
      d._notify(b, k, a);
    }));
    return this;
  }, _notify:function(a, b, c) {
    if (!b.success && this.config.loadErrorFn) {
      this.config.loadErrorFn.call(this, this, a, b, c);
    } else {
      if (a) {
        try {
          a(this, b);
        } catch (d) {
          this.error("use callback error", d, c);
        }
      }
    }
  }, _use:function(a, b) {
    this.Array || this._attach(["yui-base"]);
    var d, k, e = this, n = YUI.Env, f = n.mods, g = e.Env, h = g._used, w = n._loaderQueue, m = e.Array, l = e.config;
    k = l.bootstrap;
    var p = [], r = [], s = !0, s = l.fetchCSS, t = function(a, b) {
      a.length && m.each(a, function(a) {
        b || r.push(a);
        if (!h[a]) {
          var d = f[a], k, e;
          d ? (h[a] = !0, k = d.details.requires, e = d.details.use) : n._loaded[c][a] ? h[a] = !0 : p.push(a);
          k && k.length && t(k);
          e && e.length && t(e, 1);
        }
      });
    }, A = function(c) {
      var d = c || {success:!0, msg:"not dynamic"}, k, n = !0, f = d.data;
      e._loading = !1;
      f && (c = p, p = [], r = [], t(f), (k = p.length) && p.sort().join() == c.sort().join() && (k = !1));
      k && f ? (e._loading = !1, e._use(a, function() {
        e._attach(f) && e._notify(b, d, f);
      })) : (f && (n = e._attach(f)), n && e._notify(b, d, a));
      e._useQueue && e._useQueue.size() && !e._loading && e._use.apply(e, e._useQueue.next());
    };
    if ("*" === a[0]) {
      return(s = e._attach(e.Object.keys(f))) && A(), e;
    }
    k && e.Loader && a.length && (d = u(e), d.require(a), d.ignoreRegistered = !0, d.calculate(null, s ? null : "js"), a = d.sorted);
    t(a);
    if (d = p.length) {
      p = e.Object.keys(m.hash(p)), d = p.length;
    }
    k && d && e.Loader ? (e._loading = !0, d = u(e), d.onEnd = A, d.context = e, d.data = a, d.ignoreRegistered = !1, d.require(a), d.insert(null, s ? null : "js")) : d && e.config.use_rls ? e.Get.script(e._rls(a), {onEnd:function(a) {
      A(a);
    }, data:a}) : k && d && e.Get && !g.bootstrapped ? (e._loading = !0, k = function() {
      e._loading = !1;
      w.running = !1;
      g.bootstrapped = !0;
      e._attach(["loader"]) && e._use(a, b);
    }, n._bootstrapping ? w.add(k) : (n._bootstrapping = !0, e.Get.script(l.base + l.loaderPath, {onEnd:k}))) : (s = e._attach(a)) && A();
    return e;
  }, namespace:function() {
    for (var a = arguments, b = this, c = 0, d, k;c < a.length;c++) {
      if (d = a[c], d.indexOf(".")) {
        for (k = d.split("."), d = "YAHOO" == k[0] ? 1 : 0;d < k.length;d++) {
          b[k[d]] = b[k[d]] || {}, b = b[k[d]];
        }
      } else {
        b[d] = b[d] || {};
      }
    }
    return b;
  }, log:d, message:d, error:function(a, b, c) {
    var d;
    this.config.errorFn && (d = this.config.errorFn.apply(this, arguments));
    if (this.config.throwFail && !d) {
      throw b || Error(a);
    }
    this.message(a, "error");
    return this;
  }, guid:function(a) {
    var b = this.Env._guidp + ++this.Env._uidx;
    return a ? a + b : b;
  }, stamp:function(a, b) {
    var c;
    if (!a) {
      return a;
    }
    c = a.uniqueID && a.nodeType && 9 !== a.nodeType ? a.uniqueID : "string" === typeof a ? a : a._yuid;
    if (!c && (c = this.guid(), !b)) {
      try {
        a._yuid = c;
      } catch (d) {
        c = null;
      }
    }
    return c;
  }, destroy:function() {
    this.Event && this.Event._unload();
    delete r[this.id];
    delete this.Env;
    delete this.config;
  }};
  YUI.prototype = a;
  for (b in a) {
    a.hasOwnProperty(b) && (YUI[b] = a[b]);
  }
  YUI._init();
  g ? s(window, "load", k) : k();
  YUI.Env.add = s;
  YUI.Env.remove = t;
  "object" == typeof exports && (exports.YUI = YUI);
})();
YUI.add("yui-base", function(a) {
  function b() {
    this._init();
    this.add.apply(this, arguments);
  }
  a.Lang = a.Lang || {};
  var c = a.Lang, d = String.prototype, e = Object.prototype.toString, f = {undefined:"undefined", number:"number", "boolean":"boolean", string:"string", "[object Function]":"function", "[object RegExp]":"regexp", "[object Array]":"array", "[object Date]":"date", "[object Error]":"error"}, g = /^\s+|\s+$/g, h = /\{\s*([^\|\}]+?)\s*(?:\|([^\}]*))?\s*\}/g;
  c.isArray = function(a) {
    return "array" === c.type(a);
  };
  c.isBoolean = function(a) {
    return "boolean" === typeof a;
  };
  c.isFunction = function(a) {
    return "function" === c.type(a);
  };
  c.isDate = function(a) {
    return "date" === c.type(a) && "Invalid Date" !== a.toString() && !isNaN(a);
  };
  c.isNull = function(a) {
    return null === a;
  };
  c.isNumber = function(a) {
    return "number" === typeof a && isFinite(a);
  };
  c.isObject = function(a, b) {
    var d = typeof a;
    return a && ("object" === d || !b && ("function" === d || c.isFunction(a))) || !1;
  };
  c.isString = function(a) {
    return "string" === typeof a;
  };
  c.isUndefined = function(a) {
    return "undefined" === typeof a;
  };
  c.trim = d.trim ? function(a) {
    return a && a.trim ? a.trim() : a;
  } : function(a) {
    try {
      return a.replace(g, "");
    } catch (b) {
      return a;
    }
  };
  c.trimLeft = d.trimLeft ? function(a) {
    return a.trimLeft();
  } : function(a) {
    return a.replace(/^\s+/, "");
  };
  c.trimRight = d.trimRight ? function(a) {
    return a.trimRight();
  } : function(a) {
    return a.replace(/\s+$/, "");
  };
  c.isValue = function(a) {
    var b = c.type(a);
    switch(b) {
      case "number":
        return isFinite(a);
      case "null":
      ;
      case "undefined":
        return!1;
      default:
        return!!b;
    }
  };
  c.type = function(a) {
    return f[typeof a] || f[e.call(a)] || (a ? "object" : "null");
  };
  c.sub = function(a, b) {
    return a.replace ? a.replace(h, function(a, d) {
      return c.isUndefined(b[d]) ? a : b[d];
    }) : a;
  };
  c.now = Date.now || function() {
    return(new Date).getTime();
  };
  var m = Array.prototype, l = function(a, b, c) {
    c = c ? 2 : l.test(a);
    var d;
    b = b || 0;
    if (c) {
      try {
        return m.slice.call(a, b);
      } catch (e) {
        d = [];
        for (c = a.length;b < c;b++) {
          d.push(a[b]);
        }
        return d;
      }
    } else {
      return[a];
    }
  };
  a.Array = l;
  l.test = function(b) {
    var c = 0;
    if (a.Lang.isObject(b)) {
      if (a.Lang.isArray(b)) {
        c = 1;
      } else {
        try {
          "length" in b && !b.tagName && !b.alert && !b.apply && (c = 2);
        } catch (d) {
        }
      }
    }
    return c;
  };
  l.each = m.forEach ? function(b, c, d) {
    m.forEach.call(b || [], c, d || a);
    return a;
  } : function(b, c, d) {
    var e = b && b.length || 0, v;
    for (v = 0;v < e;v += 1) {
      c.call(d || a, b[v], v, b);
    }
    return a;
  };
  l.hash = function(a, b) {
    var c = {}, d = a.length, e = b && b.length, f;
    for (f = 0;f < d;f += 1) {
      c[a[f]] = e && e > f ? b[f] : !0;
    }
    return c;
  };
  l.indexOf = m.indexOf ? function(a, b) {
    return m.indexOf.call(a, b);
  } : function(a, b) {
    for (var c = 0;c < a.length;c += 1) {
      if (a[c] === b) {
        return c;
      }
    }
    return-1;
  };
  l.numericSort = function(a, b) {
    return a - b;
  };
  l.some = m.some ? function(a, b, c) {
    return m.some.call(a, b, c);
  } : function(a, b, c) {
    var d = a.length, e;
    for (e = 0;e < d;e += 1) {
      if (b.call(c, a[e], e, a)) {
        return!0;
      }
    }
    return!1;
  };
  b.prototype = {_init:function() {
    this._q = [];
  }, next:function() {
    return this._q.shift();
  }, last:function() {
    return this._q.pop();
  }, add:function() {
    this._q.push.apply(this._q, arguments);
    return this;
  }, size:function() {
    return this._q.length;
  }};
  a.Queue = b;
  YUI.Env._loaderQueue = YUI.Env._loaderQueue || new b;
  a.merge = function() {
    var b = arguments, c = {}, d, e = b.length;
    for (d = 0;d < e;d += 1) {
      a.mix(c, b[d], !0);
    }
    return c;
  };
  a.mix = function(b, c, d, e, v, f) {
    if (!c || !b) {
      return b || a;
    }
    if (v) {
      switch(v) {
        case 1:
          return a.mix(b.prototype, c.prototype, d, e, 0, f);
        case 2:
          a.mix(b.prototype, c.prototype, d, e, 0, f);
          break;
        case 3:
          return a.mix(b, c.prototype, d, e, 0, f);
        case 4:
          return a.mix(b.prototype, c, d, e, 0, f);
      }
    }
    var g, q, h;
    if (e && e.length) {
      for (g = 0, v = e.length;g < v;++g) {
        q = e[g], h = a.Lang.type(b[q]), c.hasOwnProperty(q) && (f && "object" == h ? a.mix(b[q], c[q]) : !d && q in b || (b[q] = c[q]));
      }
    } else {
      for (g in c) {
        c.hasOwnProperty(g) && (f && a.Lang.isObject(b[g], !0) ? a.mix(b[g], c[g], d, e, 0, !0) : !d && g in b || (b[g] = c[g]));
      }
      a.UA.ie && (c = c.toString, a.Lang.isFunction(c) && c != Object.prototype.toString && (b.toString = c));
    }
    return b;
  };
  a.cached = function(a, b, c) {
    b = b || {};
    return function(d) {
      var e = 1 < arguments.length ? Array.prototype.join.call(arguments, "__") : d;
      if (!(e in b) || c && b[e] == c) {
        b[e] = a.apply(a, arguments);
      }
      return b[e];
    };
  };
  var r = function() {
  }, p = function(a) {
    r.prototype = a;
    return new r;
  }, s = function(a, b) {
    return a && a.hasOwnProperty && a.hasOwnProperty(b);
  }, t = function(a, b) {
    var c = 2 === b, d = c ? 0 : [], e;
    for (e in a) {
      s(a, e) && (c ? d++ : d.push(b ? a[e] : e));
    }
    return d;
  };
  a.Object = p;
  p.keys = function(a) {
    return t(a);
  };
  p.values = function(a) {
    return t(a, 1);
  };
  p.size = Object.size || function(a) {
    return t(a, 2);
  };
  p.hasKey = s;
  p.hasValue = function(b, c) {
    return-1 < a.Array.indexOf(p.values(b), c);
  };
  p.owns = s;
  p.each = function(b, c, d, e) {
    d = d || a;
    for (var f in b) {
      (e || s(b, f)) && c.call(d, b[f], f, b);
    }
    return a;
  };
  p.some = function(b, c, d, e) {
    d = d || a;
    for (var f in b) {
      if ((e || s(b, f)) && c.call(d, b[f], f, b)) {
        return!0;
      }
    }
    return!1;
  };
  p.getValue = function(b, c) {
    if (a.Lang.isObject(b)) {
      var d, e = a.Array(c), f = e.length;
      for (d = 0;void 0 !== b && d < f;d++) {
        b = b[e[d]];
      }
      return b;
    }
  };
  p.setValue = function(b, c, d) {
    var e = a.Array(c), f = e.length - 1, g = b;
    if (0 <= f) {
      for (c = 0;void 0 !== g && c < f;c++) {
        g = g[e[c]];
      }
      if (void 0 !== g) {
        g[e[c]] = d;
      } else {
        return;
      }
    }
    return b;
  };
  p.isEmpty = function(a) {
    for (var b in a) {
      if (s(a, b)) {
        return!1;
      }
    }
    return!0;
  };
  YUI.Env.parseUA = function(b) {
    var c = function(a) {
      var b = 0;
      return parseFloat(a.replace(/\./g, function() {
        return 1 == b++ ? "" : ".";
      }));
    }, d = a.config.win, e = d && d.navigator, f = {ie:0, opera:0, gecko:0, webkit:0, chrome:0, mobile:null, air:0, ipad:0, iphone:0, ipod:0, ios:null, android:0, webos:0, caja:e && e.cajaVersion, secure:!1, os:null};
    b = b || e && e.userAgent;
    d = (d = d && d.location) && d.href;
    f.secure = d && 0 === d.toLowerCase().indexOf("https");
    if (b) {
      /windows|win32/i.test(b) ? f.os = "windows" : /macintosh/i.test(b) ? f.os = "macintosh" : /rhino/i.test(b) && (f.os = "rhino");
      /KHTML/.test(b) && (f.webkit = 1);
      if ((d = b.match(/AppleWebKit\/([^\s]*)/)) && d[1]) {
        f.webkit = c(d[1]);
        if (/ Mobile\//.test(b)) {
          f.mobile = "Apple", (d = b.match(/OS ([^\s]*)/)) && d[1] && (d = c(d[1].replace("_", "."))), f.ios = d, f.ipad = f.ipod = f.iphone = 0, (d = b.match(/iPad|iPod|iPhone/)) && d[0] && (f[d[0].toLowerCase()] = f.ios);
        } else {
          if (d = b.match(/NokiaN[^\/]*|Android \d\.\d|webOS\/\d\.\d/)) {
            f.mobile = d[0];
          }
          /webOS/.test(b) && (f.mobile = "WebOS", (d = b.match(/webOS\/([^\s]*);/)) && d[1] && (f.webos = c(d[1])));
          / Android/.test(b) && (f.mobile = "Android", (d = b.match(/Android ([^\s]*);/)) && d[1] && (f.android = c(d[1])));
        }
        if ((d = b.match(/Chrome\/([^\s]*)/)) && d[1]) {
          f.chrome = c(d[1]);
        } else {
          if (d = b.match(/AdobeAIR\/([^\s]*)/)) {
            f.air = d[0];
          }
        }
      }
      if (!f.webkit) {
        if ((d = b.match(/Opera[\s\/]([^\s]*)/)) && d[1]) {
          if (f.opera = c(d[1]), d = b.match(/Opera Mini[^;]*/)) {
            f.mobile = d[0];
          }
        } else {
          if ((d = b.match(/MSIE\s([^;]*)/)) && d[1]) {
            f.ie = c(d[1]);
          } else {
            if (d = b.match(/Gecko\/([^\s]*)/)) {
              f.gecko = 1, (d = b.match(/rv:([^\s\)]*)/)) && d[1] && (f.gecko = c(d[1]));
            }
          }
        }
      }
    }
    return YUI.Env.UA = f;
  };
  a.UA = YUI.Env.UA || YUI.Env.parseUA();
}, "3.3.0");
YUI.add("get", function(a) {
  var b = a.UA, c = a.Lang;
  a.Get = function() {
    var d, e, f, g = {}, h = 0, m, l = function(b, c, d) {
      b = (d || a.config.win).document.createElement(b);
      for (var e in c) {
        c[e] && c.hasOwnProperty(e) && b.setAttribute(e, c[e]);
      }
      return b;
    }, r = function(b, c, d) {
      b = {id:a.guid(), type:"text/css", rel:"stylesheet", href:b};
      d && a.mix(b, d);
      return l("link", b, c);
    }, p = function(b, c, d) {
      var e = {id:a.guid(), type:"text/javascript"};
      d && a.mix(e, d);
      e.src = b;
      return l("script", e, c);
    }, s = function(a, b, c) {
      return{tId:a.tId, win:a.win, data:a.data, nodes:a.nodes, msg:b, statusText:c, purge:function() {
        e(this.tId);
      }};
    }, t = function(a, b, c) {
      a = g[a];
      var d;
      a && a.onEnd && (d = a.context || a, a.onEnd.call(d, s(a, b, c)));
    }, k = function(a, b) {
      var c = g[a], d;
      c.timer && clearTimeout(c.timer);
      c.onFailure && (d = c.context || c, c.onFailure.call(d, s(c, b)));
      t(a, b, "failure");
    }, u = function(a) {
      var b = g[a], c, d;
      b.timer && clearTimeout(b.timer);
      b.finished = !0;
      b.aborted ? k(a, "transaction " + a + " was aborted") : (b.onSuccess && (d = b.context || b, b.onSuccess.call(d, s(b))), t(a, c, "OK"));
    }, n = function(a, e) {
      var h = g[a], q, m, l, z, x;
      h.timer && clearTimeout(h.timer);
      if (h.aborted) {
        k(a, "transaction " + a + " was aborted");
      } else {
        if (e ? (h.url.shift(), h.varName && h.varName.shift()) : (h.url = c.isString(h.url) ? [h.url] : h.url, h.varName && (h.varName = c.isString(h.varName) ? [h.varName] : h.varName)), q = h.win, m = q.document, l = m.getElementsByTagName("head")[0], 0 === h.url.length) {
          u(a);
        } else {
          x = h.url[0];
          if (!x) {
            return h.url.shift(), n(a);
          }
          h.timeout && (h.timer = setTimeout(function() {
            var b = g[a], c;
            b.onTimeout && (c = b.context || b, b.onTimeout.call(c, s(b)));
            t(a, "timeout", "timeout");
          }, h.timeout));
          z = "script" === h.type ? p(x, q, h.attributes) : r(x, q, h.attributes);
          f(h.type, z, a, x, q, h.url.length);
          h.nodes.push(z);
          (q = h.insertBefore || m.getElementsByTagName("base")[0]) ? (l = d(q, a)) && l.parentNode.insertBefore(z, l) : l.appendChild(z);
          (b.webkit || b.gecko) && "css" === h.type && n(a, x);
        }
      }
    }, w = function(b, c, d) {
      d = d || {};
      var f = "q" + h++;
      if (0 === h % (d.purgethreshold || a.Get.PURGE_THRESH) && !m) {
        m = !0;
        var k, l;
        for (k in g) {
          g.hasOwnProperty(k) && (l = g[k], l.autopurge && l.finished && (e(l.tId), delete g[k]));
        }
        m = !1;
      }
      g[f] = a.merge(d, {tId:f, type:b, url:c, finished:!1, nodes:[]});
      c = g[f];
      c.win = c.win || a.config.win;
      c.context = c.context || c;
      c.autopurge = "autopurge" in c ? c.autopurge : "script" === b ? !0 : !1;
      c.attributes = c.attributes || {};
      c.attributes.charset = d.charset || c.attributes.charset || "utf-8";
      n(f);
      return{tId:f};
    };
    f = function(a, c, d, e, f, g, h) {
      var l = h || n;
      b.ie ? c.onreadystatechange = function() {
        var a = this.readyState;
        if ("loaded" === a || "complete" === a) {
          c.onreadystatechange = null, l(d, e);
        }
      } : b.webkit ? "script" === a && c.addEventListener("load", function() {
        l(d, e);
      }) : (c.onload = function() {
        l(d, e);
      }, c.onerror = function(a) {
        k(d, a + ": " + e);
      });
    };
    d = function(a, b) {
      var d = g[b];
      (d = c.isString(a) ? d.win.document.getElementById(a) : a) || k(b, "target node not found: " + a);
      return d;
    };
    e = function(a) {
      var b, c, e, f, h, k = g[a];
      if (k) {
        b = k.nodes;
        c = b.length;
        e = k.win.document;
        f = e.getElementsByTagName("head")[0];
        if (e = k.insertBefore || e.getElementsByTagName("base")[0]) {
          if (a = d(e, a)) {
            f = a.parentNode;
          }
        }
        for (a = 0;a < c;a += 1) {
          e = b[a];
          if (e.clearAttributes) {
            e.clearAttributes();
          } else {
            for (h in e) {
              e.hasOwnProperty(h) && delete e[h];
            }
          }
          f.removeChild(e);
        }
      }
      k.nodes = [];
    };
    return{PURGE_THRESH:20, _finalize:function(a) {
      setTimeout(function() {
        u(a);
      }, 0);
    }, abort:function(a) {
      a = c.isString(a) ? a : a.tId;
      if (a = g[a]) {
        a.aborted = !0;
      }
    }, script:function(a, b) {
      return w("script", a, b);
    }, css:function(a, b) {
      return w("css", a, b);
    }};
  }();
}, "3.3.0", {requires:["yui-base"]});
YUI.add("features", function(a) {
  var b = {};
  a.mix(a.namespace("Features"), {tests:b, add:function(a, c, f) {
    b[a] = b[a] || {};
    b[a][c] = f;
  }, all:function(c, e) {
    var f = b[c], g = "";
    f && a.Object.each(f, function(b, f) {
      g += f + ":" + (a.Features.test(c, f, e) ? 1 : 0) + ";";
    });
    return g;
  }, test:function(c, e, f) {
    f = f || [];
    var g, h = (c = b[c]) && c[e];
    h && (g = h.result, a.Lang.isUndefined(g) && ((e = h.ua) && (g = a.UA[e]), c = h.test, !c || e && !g || (g = c.apply(a, f)), h.result = g));
    return g;
  }});
  var c = a.Features.add;
  c("load", "0", {test:function(a) {
    return!(a.UA.ios || a.UA.android);
  }, trigger:"autocomplete-list"});
  c("load", "1", {test:function(a) {
    var b = a.Features.test, c = a.Features.add, g = a.config.win, h = a.config.doc;
    a = !1;
    c("style", "computedStyle", {test:function() {
      return g && "getComputedStyle" in g;
    }});
    c("style", "opacity", {test:function() {
      return h && "opacity" in h.documentElement.style;
    }});
    return a = !b("style", "opacity") && !b("style", "computedStyle");
  }, trigger:"dom-style"});
  c("load", "2", {trigger:"widget-base", ua:"ie"});
  c("load", "3", {test:function(a) {
    return(a = a.config.doc && a.config.doc.implementation) && !a.hasFeature("Events", "2.0");
  }, trigger:"node-base"});
  c("load", "4", {test:function(a) {
    return a.config.win && "ontouchstart" in a.config.win && !a.UA.chrome;
  }, trigger:"dd-drag"});
  c("load", "5", {test:function(a) {
    var b = a.config.doc.documentMode;
    return a.UA.ie && (!("onhashchange" in a.config.win) || !b || 8 > b);
  }, trigger:"history-hash"});
}, "3.3.0", {requires:["yui-base"]});
YUI.add("rls", function(a) {
  a._rls = function(b) {
    var c = a.config, d = c.rls || {m:1, v:a.version, gv:c.gallery, env:1, lang:c.lang, "2in3v":c["2in3"], "2v":c.yui2, filt:c.filter, filts:c.filters, tests:1}, e = c.rls_base || "load?", f;
    if (!(f = c.rls_tmpl)) {
      f = "";
      for (var g in d) {
        g in d && d[g] && (f += g + "={" + g + "}&");
      }
    }
    g = f;
    d.m = b;
    d.env = a.Object.keys(YUI.Env.mods);
    d.tests = a.Features.all("load", [a]);
    b = a.Lang.sub(e + g, d);
    c.rls = d;
    c.rls_tmpl = g;
    return b;
  };
}, "3.3.0", {requires:["get", "features"]});
YUI.add("intl-base", function(a) {
  var b = /[, ]/;
  a.mix(a.namespace("Intl"), {lookupBestLang:function(c, d) {
    var e, f, g;
    a.Lang.isString(c) && (c = c.split(b));
    for (e = 0;e < c.length;e += 1) {
      if ((f = c[e]) && "*" !== f) {
        for (;0 < f.length;) {
          a: {
            g = f;
            for (var h = void 0, h = 0;h < d.length;h += 1) {
              if (g.toLowerCase() === d[h].toLowerCase()) {
                g = d[h];
                break a;
              }
            }
            g = void 0;
          }
          if (g) {
            return g;
          }
          g = f.lastIndexOf("-");
          if (0 <= g) {
            f = f.substring(0, g), 2 <= g && "-" === f.charAt(g - 2) && (f = f.substring(0, g - 2));
          } else {
            break;
          }
        }
      }
    }
    return "";
  }});
}, "3.3.0", {requires:["yui-base"]});
YUI.add("yui-log", function(a) {
  var b = {debug:1, info:1, warn:1, error:1};
  a.log = function(c, d, e, f) {
    var g, h, m, l;
    l = a.config;
    var r = a.fire ? a : YUI.Env.globalEvents;
    l.debug && (e && (h = l.logExclude, m = l.logInclude, !m || e in m ? h && e in h && (g = 1) : g = 1), g || (l.useBrowserConsole && (g = e ? e + ": " + c : c, a.Lang.isFunction(l.logFn) ? l.logFn.call(a, c, d, e) : "undefined" != typeof console && console.log ? (l = d && console[d] && d in b ? d : "log", console[l](g)) : "undefined" != typeof opera && opera.postError(g)), r && !f && (r != a || r.getEvent("yui:log") || r.publish("yui:log", {broadcast:2}), r.fire("yui:log", {msg:c, cat:d, src:e}))));
    return a;
  };
  a.message = function() {
    return a.log.apply(a, arguments);
  };
}, "3.3.0", {requires:["yui-base"]});
YUI.add("yui-later", function(a) {
  a.later = function(b, c, d, e, f) {
    b = b || 0;
    var g = d, h;
    c && a.Lang.isString(d) && (g = c[d]);
    d = a.Lang.isUndefined(e) ? function() {
      g.call(c);
    } : function() {
      g.apply(c, a.Array(e));
    };
    h = f ? setInterval(d, b) : setTimeout(d, b);
    return{id:h, interval:f, cancel:function() {
      this.interval ? clearInterval(h) : clearTimeout(h);
    }};
  };
  a.Lang.later = a.later;
}, "3.3.0", {requires:["yui-base"]});
YUI.add("yui-throttle", function(a) {
  a.throttle = function(b, c) {
    c = c ? c : a.config.throttleTime || 150;
    if (-1 === c) {
      return function() {
        b.apply(null, arguments);
      };
    }
    var d = a.Lang.now();
    return function() {
      var e = a.Lang.now();
      e - d > c && (d = e, b.apply(null, arguments));
    };
  };
}, "3.3.0", {requires:["yui-base"]});
YUI.add("yui", function(a) {
}, "3.3.0", {use:"yui-base get features rls intl-base yui-log yui-later yui-throttle".split(" ")});
// Input 1
var webfont = {bind:function(a, b, c) {
  var d = 2 < arguments.length ? Array.prototype.slice.call(arguments, 2) : [];
  return function() {
    d.push.apply(d, arguments);
    return b.apply(a, d);
  };
}, extendsClass:function(a, b) {
  function c() {
  }
  c.prototype = a.prototype;
  b.prototype = new c;
  b.prototype.constructor = b;
  b.superCtor_ = a;
  b.super_ = a.prototype;
}, DomHelper:function(a) {
  this.document_ = a;
  this.supportForStyle_ = void 0;
}};
webfont.DomHelper.prototype.createElement = function(a, b, c) {
  a = this.document_.createElement(a);
  if (b) {
    for (var d in b) {
      b.hasOwnProperty(d) && ("style" == d ? this.setStyle(a, b[d]) : a.setAttribute(d, b[d]));
    }
  }
  c && a.appendChild(this.document_.createTextNode(c));
  return a;
};
webfont.DomHelper.prototype.insertInto = function(a, b) {
  var c = this.document_.getElementsByTagName(a)[0];
  c || (c = document.documentElement);
  return c && c.lastChild ? (c.insertBefore(b, c.lastChild), !0) : !1;
};
webfont.DomHelper.prototype.whenBodyExists = function(a) {
  var b = function() {
    document.body ? a() : setTimeout(b, 0);
  };
  b();
};
webfont.DomHelper.prototype.removeElement = function(a) {
  return a.parentNode ? (a.parentNode.removeChild(a), !0) : !1;
};
webfont.DomHelper.prototype.createCssLink = function(a) {
  return this.createElement("link", {rel:"stylesheet", href:a});
};
webfont.DomHelper.prototype.createScriptSrc = function(a) {
  return this.createElement("script", {src:a});
};
webfont.DomHelper.prototype.appendClassName = function(a, b) {
  for (var c = a.className.split(/\s+/), d = 0, e = c.length;d < e;d++) {
    if (c[d] == b) {
      return;
    }
  }
  c.push(b);
  a.className = c.join(" ").replace(/^\s+/, "");
};
webfont.DomHelper.prototype.removeClassName = function(a, b) {
  for (var c = a.className.split(/\s+/), d = [], e = 0, f = c.length;e < f;e++) {
    c[e] != b && d.push(c[e]);
  }
  a.className = d.join(" ").replace(/^\s+/, "").replace(/\s+$/, "");
};
webfont.DomHelper.prototype.hasClassName = function(a, b) {
  for (var c = a.className.split(/\s+/), d = 0, e = c.length;d < e;d++) {
    if (c[d] == b) {
      return!0;
    }
  }
  return!1;
};
webfont.DomHelper.prototype.setStyle = function(a, b) {
  this.hasSupportForStyle_() ? a.setAttribute("style", b) : a.style.cssText = b;
};
webfont.DomHelper.prototype.hasSupportForStyle_ = function() {
  if (void 0 === this.supportForStyle_) {
    var a = this.document_.createElement("p");
    a.innerHTML = '<a style="top:1px;">w</a>';
    this.supportForStyle_ = /top/.test(a.getElementsByTagName("a")[0].getAttribute("style"));
  }
  return this.supportForStyle_;
};
webfont.UserAgent = function(a, b, c, d, e, f, g, h) {
  this.name_ = a;
  this.version_ = b;
  this.engine_ = c;
  this.engineVersion_ = d;
  this.platform_ = e;
  this.platformVersion_ = f;
  this.documentMode_ = g;
  this.webFontSupport_ = h;
};
webfont.UserAgent.prototype.getName = function() {
  return this.name_;
};
webfont.UserAgent.prototype.getVersion = function() {
  return this.version_;
};
webfont.UserAgent.prototype.getEngine = function() {
  return this.engine_;
};
webfont.UserAgent.prototype.getEngineVersion = function() {
  return this.engineVersion_;
};
webfont.UserAgent.prototype.getPlatform = function() {
  return this.platform_;
};
webfont.UserAgent.prototype.getPlatformVersion = function() {
  return this.platformVersion_;
};
webfont.UserAgent.prototype.getDocumentMode = function() {
  return this.documentMode_;
};
webfont.UserAgent.prototype.isSupportingWebFont = function() {
  return this.webFontSupport_;
};
webfont.UserAgentParser = function(a, b) {
  this.userAgent_ = a;
  this.doc_ = b;
};
webfont.UserAgentParser.UNKNOWN = "Unknown";
webfont.UserAgentParser.UNKNOWN_USER_AGENT = new webfont.UserAgent(webfont.UserAgentParser.UNKNOWN, webfont.UserAgentParser.UNKNOWN, webfont.UserAgentParser.UNKNOWN, webfont.UserAgentParser.UNKNOWN, webfont.UserAgentParser.UNKNOWN, webfont.UserAgentParser.UNKNOWN, void 0, !1);
webfont.UserAgentParser.prototype.parse = function() {
  return this.isIe_() ? this.parseIeUserAgentString_() : this.isOpera_() ? this.parseOperaUserAgentString_() : this.isWebKit_() ? this.parseWebKitUserAgentString_() : this.isGecko_() ? this.parseGeckoUserAgentString_() : webfont.UserAgentParser.UNKNOWN_USER_AGENT;
};
webfont.UserAgentParser.prototype.getPlatform_ = function() {
  var a = this.getMatchingGroup_(this.userAgent_, /(iPod|iPad|iPhone|Android)/, 1);
  if ("" != a) {
    return a;
  }
  a = this.getMatchingGroup_(this.userAgent_, /(Linux|Mac_PowerPC|Macintosh|Windows|CrOS)/, 1);
  return "" != a ? ("Mac_PowerPC" == a && (a = "Macintosh"), a) : webfont.UserAgentParser.UNKNOWN;
};
webfont.UserAgentParser.prototype.getPlatformVersion_ = function() {
  var a = this.getMatchingGroup_(this.userAgent_, /(OS X|Windows NT|Android|CrOS) ([^;)]+)/, 2);
  return a || (a = this.getMatchingGroup_(this.userAgent_, /(iPhone )?OS ([\d_]+)/, 2)) ? a : (a = this.getMatchingGroup_(this.userAgent_, /Linux ([i\d]+)/, 1)) ? a : webfont.UserAgentParser.UNKNOWN;
};
webfont.UserAgentParser.prototype.isIe_ = function() {
  return-1 != this.userAgent_.indexOf("MSIE");
};
webfont.UserAgentParser.prototype.parseIeUserAgentString_ = function() {
  var a = this.getMatchingGroup_(this.userAgent_, /(MSIE [\d\w\.]+)/, 1);
  if ("" != a) {
    var b = a.split(" "), a = b[0], b = b[1];
    return new webfont.UserAgent(a, b, a, b, this.getPlatform_(), this.getPlatformVersion_(), this.getDocumentMode_(this.doc_), 6 <= this.getMajorVersion_(b));
  }
  return new webfont.UserAgent("MSIE", webfont.UserAgentParser.UNKNOWN, "MSIE", webfont.UserAgentParser.UNKNOWN, this.getPlatform_(), this.getPlatformVersion_(), this.getDocumentMode_(this.doc_), !1);
};
webfont.UserAgentParser.prototype.isOpera_ = function() {
  return-1 != this.userAgent_.indexOf("Opera");
};
webfont.UserAgentParser.prototype.parseOperaUserAgentString_ = function() {
  var a = webfont.UserAgentParser.UNKNOWN, b = webfont.UserAgentParser.UNKNOWN, c = this.getMatchingGroup_(this.userAgent_, /(Presto\/[\d\w\.]+)/, 1);
  "" != c ? (b = c.split("/"), a = b[0], b = b[1]) : (-1 != this.userAgent_.indexOf("Gecko") && (a = "Gecko"), c = this.getMatchingGroup_(this.userAgent_, /rv:([^\)]+)/, 1), "" != c && (b = c));
  if (-1 != this.userAgent_.indexOf("Version/") && (c = this.getMatchingGroup_(this.userAgent_, /Version\/([\d\.]+)/, 1), "" != c)) {
    return new webfont.UserAgent("Opera", c, a, b, this.getPlatform_(), this.getPlatformVersion_(), this.getDocumentMode_(this.doc_), 10 <= this.getMajorVersion_(c));
  }
  c = this.getMatchingGroup_(this.userAgent_, /Opera[\/ ]([\d\.]+)/, 1);
  return "" != c ? new webfont.UserAgent("Opera", c, a, b, this.getPlatform_(), this.getPlatformVersion_(), this.getDocumentMode_(this.doc_), 10 <= this.getMajorVersion_(c)) : new webfont.UserAgent("Opera", webfont.UserAgentParser.UNKNOWN, a, b, this.getPlatform_(), this.getPlatformVersion_(), this.getDocumentMode_(this.doc_), !1);
};
webfont.UserAgentParser.prototype.isWebKit_ = function() {
  return-1 != this.userAgent_.indexOf("AppleWebKit");
};
webfont.UserAgentParser.prototype.parseWebKitUserAgentString_ = function() {
  var a = this.getPlatform_(), b = this.getPlatformVersion_(), c = this.getMatchingGroup_(this.userAgent_, /AppleWebKit\/([\d\.\+]+)/, 1);
  "" == c && (c = webfont.UserAgentParser.UNKNOWN);
  var d = webfont.UserAgentParser.UNKNOWN;
  -1 != this.userAgent_.indexOf("Chrome") || -1 != this.userAgent_.indexOf("CrMo") ? d = "Chrome" : -1 != this.userAgent_.indexOf("Safari") ? d = "Safari" : -1 != this.userAgent_.indexOf("AdobeAIR") && (d = "AdobeAIR");
  var e = webfont.UserAgentParser.UNKNOWN;
  -1 != this.userAgent_.indexOf("Version/") ? e = this.getMatchingGroup_(this.userAgent_, /Version\/([\d\.\w]+)/, 1) : "Chrome" == d ? e = this.getMatchingGroup_(this.userAgent_, /(Chrome|CrMo)\/([\d\.]+)/, 2) : "AdobeAIR" == d && (e = this.getMatchingGroup_(this.userAgent_, /AdobeAIR\/([\d\.]+)/, 1));
  var f = !1;
  "AdobeAIR" == d ? (f = this.getMatchingGroup_(e, /\d+\.(\d+)/, 1), f = 2 < this.getMajorVersion_(e) || 2 == this.getMajorVersion_(e) && 5 <= parseInt(f, 10)) : (f = this.getMatchingGroup_(c, /\d+\.(\d+)/, 1), f = 526 <= this.getMajorVersion_(c) || 525 <= this.getMajorVersion_(c) && 13 <= parseInt(f, 10));
  return new webfont.UserAgent(d, e, "AppleWebKit", c, a, b, this.getDocumentMode_(this.doc_), f);
};
webfont.UserAgentParser.prototype.isGecko_ = function() {
  return-1 != this.userAgent_.indexOf("Gecko");
};
webfont.UserAgentParser.prototype.parseGeckoUserAgentString_ = function() {
  var a = webfont.UserAgentParser.UNKNOWN, b = webfont.UserAgentParser.UNKNOWN, c = !1;
  if (-1 != this.userAgent_.indexOf("Firefox")) {
    var a = "Firefox", d = this.getMatchingGroup_(this.userAgent_, /Firefox\/([\d\w\.]+)/, 1);
    "" != d && (c = this.getMatchingGroup_(d, /\d+\.(\d+)/, 1), b = d, c = "" != d && 3 <= this.getMajorVersion_(d) && 5 <= parseInt(c, 10));
  } else {
    -1 != this.userAgent_.indexOf("Mozilla") && (a = "Mozilla");
  }
  d = this.getMatchingGroup_(this.userAgent_, /rv:([^\)]+)/, 1);
  if ("" == d) {
    d = webfont.UserAgentParser.UNKNOWN;
  } else {
    if (!c) {
      var c = this.getMajorVersion_(d), e = parseInt(this.getMatchingGroup_(d, /\d+\.(\d+)/, 1), 10), f = parseInt(this.getMatchingGroup_(d, /\d+\.\d+\.(\d+)/, 1), 10), c = 1 < c || 1 == c && 9 < e || 1 == c && 9 == e && 2 <= f || null != d.match(/1\.9\.1b[123]/) || null != d.match(/1\.9\.1\.[\d\.]+/)
    }
  }
  return new webfont.UserAgent(a, b, "Gecko", d, this.getPlatform_(), this.getPlatformVersion_(), this.getDocumentMode_(this.doc_), c);
};
webfont.UserAgentParser.prototype.getMajorVersion_ = function(a) {
  a = this.getMatchingGroup_(a, /(\d+)/, 1);
  return "" != a ? parseInt(a, 10) : -1;
};
webfont.UserAgentParser.prototype.getMatchingGroup_ = function(a, b, c) {
  return(a = a.match(b)) && a[c] ? a[c] : "";
};
webfont.UserAgentParser.prototype.getDocumentMode_ = function(a) {
  if (a.documentMode) {
    return a.documentMode;
  }
};
webfont.EventDispatcher = function(a, b, c, d) {
  this.domHelper_ = a;
  this.htmlElement_ = b;
  this.callbacks_ = c;
  this.namespace_ = d || webfont.EventDispatcher.DEFAULT_NAMESPACE;
  this.cssClassName_ = new webfont.CssClassName("-");
};
webfont.EventDispatcher.DEFAULT_NAMESPACE = "wf";
webfont.EventDispatcher.LOADING = "loading";
webfont.EventDispatcher.ACTIVE = "active";
webfont.EventDispatcher.INACTIVE = "inactive";
webfont.EventDispatcher.FONT = "font";
webfont.EventDispatcher.prototype.dispatchLoading = function() {
  this.domHelper_.appendClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, webfont.EventDispatcher.LOADING));
  this.dispatch_(webfont.EventDispatcher.LOADING);
};
webfont.EventDispatcher.prototype.dispatchFontLoading = function(a, b) {
  this.domHelper_.appendClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, a, b, webfont.EventDispatcher.LOADING));
  this.dispatch_(webfont.EventDispatcher.FONT + webfont.EventDispatcher.LOADING, a, b);
};
webfont.EventDispatcher.prototype.dispatchFontActive = function(a, b) {
  this.domHelper_.removeClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, a, b, webfont.EventDispatcher.LOADING));
  this.domHelper_.removeClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, a, b, webfont.EventDispatcher.INACTIVE));
  this.domHelper_.appendClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, a, b, webfont.EventDispatcher.ACTIVE));
  this.dispatch_(webfont.EventDispatcher.FONT + webfont.EventDispatcher.ACTIVE, a, b);
};
webfont.EventDispatcher.prototype.dispatchFontInactive = function(a, b) {
  this.domHelper_.removeClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, a, b, webfont.EventDispatcher.LOADING));
  this.domHelper_.hasClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, a, b, webfont.EventDispatcher.ACTIVE)) || this.domHelper_.appendClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, a, b, webfont.EventDispatcher.INACTIVE));
  this.dispatch_(webfont.EventDispatcher.FONT + webfont.EventDispatcher.INACTIVE, a, b);
};
webfont.EventDispatcher.prototype.dispatchInactive = function() {
  this.domHelper_.removeClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, webfont.EventDispatcher.LOADING));
  this.domHelper_.hasClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, webfont.EventDispatcher.ACTIVE)) || this.domHelper_.appendClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, webfont.EventDispatcher.INACTIVE));
  this.dispatch_(webfont.EventDispatcher.INACTIVE);
};
webfont.EventDispatcher.prototype.dispatchActive = function() {
  this.domHelper_.removeClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, webfont.EventDispatcher.LOADING));
  this.domHelper_.removeClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, webfont.EventDispatcher.INACTIVE));
  this.domHelper_.appendClassName(this.htmlElement_, this.cssClassName_.build(this.namespace_, webfont.EventDispatcher.ACTIVE));
  this.dispatch_(webfont.EventDispatcher.ACTIVE);
};
webfont.EventDispatcher.prototype.dispatch_ = function(a, b, c) {
  if (this.callbacks_[a]) {
    this.callbacks_[a](b, c);
  }
};
webfont.FontModuleLoader = function() {
  this.modules_ = {};
};
webfont.FontModuleLoader.prototype.addModuleFactory = function(a, b) {
  this.modules_[a] = b;
};
webfont.FontModuleLoader.prototype.getModules = function(a) {
  var b = [], c;
  for (c in a) {
    if (a.hasOwnProperty(c)) {
      var d = this.modules_[c];
      d && b.push(d(a[c]));
    }
  }
  return b;
};
webfont.FontWatcher = function(a, b, c, d, e) {
  this.domHelper_ = a;
  this.eventDispatcher_ = b;
  this.fontSizer_ = c;
  this.asyncCall_ = d;
  this.getTime_ = e;
  this.currentlyWatched_ = 0;
  this.success_ = this.last_ = !1;
};
webfont.FontWatcher.DEFAULT_VARIATION = "n4";
webfont.FontWatcher.prototype.watch = function(a, b, c, d, e) {
  for (var f = a.length, g = 0;g < f;g++) {
    var h = a[g];
    b[h] || (b[h] = [webfont.FontWatcher.DEFAULT_VARIATION]);
    this.currentlyWatched_ += b[h].length;
  }
  e && (this.last_ = e);
  for (g = 0;g < f;g++) {
    h = a[g];
    e = b[h];
    for (var m = c[h], l = 0, r = e.length;l < r;l++) {
      var p = e[l];
      this.eventDispatcher_.dispatchFontLoading(h, p);
      var s = webfont.bind(this, this.fontActive_), t = webfont.bind(this, this.fontInactive_);
      (new d(s, t, this.domHelper_, this.fontSizer_, this.asyncCall_, this.getTime_, h, p, m)).start();
    }
  }
};
webfont.FontWatcher.prototype.fontActive_ = function(a, b) {
  this.eventDispatcher_.dispatchFontActive(a, b);
  this.success_ = !0;
  this.decreaseCurrentlyWatched_();
};
webfont.FontWatcher.prototype.fontInactive_ = function(a, b) {
  this.eventDispatcher_.dispatchFontInactive(a, b);
  this.decreaseCurrentlyWatched_();
};
webfont.FontWatcher.prototype.decreaseCurrentlyWatched_ = function() {
  0 == --this.currentlyWatched_ && this.last_ && (this.success_ ? this.eventDispatcher_.dispatchActive() : this.eventDispatcher_.dispatchInactive());
};
webfont.FontWatchRunner = function(a, b, c, d, e, f, g, h, m) {
  this.activeCallback_ = a;
  this.inactiveCallback_ = b;
  this.domHelper_ = c;
  this.fontSizer_ = d;
  this.asyncCall_ = e;
  this.getTime_ = f;
  this.nameHelper_ = new webfont.CssFontFamilyName;
  this.fvd_ = new webfont.FontVariationDescription;
  this.fontFamily_ = g;
  this.fontDescription_ = h;
  this.fontTestString_ = m || webfont.FontWatchRunner.DEFAULT_TEST_STRING;
  this.originalSizeA_ = this.getDefaultFontSize_(webfont.FontWatchRunner.DEFAULT_FONTS_A);
  this.originalSizeB_ = this.getDefaultFontSize_(webfont.FontWatchRunner.DEFAULT_FONTS_B);
  this.lastObservedSizeA_ = this.originalSizeA_;
  this.lastObservedSizeB_ = this.originalSizeB_;
  this.requestedFontA_ = this.createHiddenElementWithFont_(webfont.FontWatchRunner.DEFAULT_FONTS_A);
  this.requestedFontB_ = this.createHiddenElementWithFont_(webfont.FontWatchRunner.DEFAULT_FONTS_B);
};
webfont.FontWatchRunner.DEFAULT_FONTS_A = "arial,'URW Gothic L',sans-serif";
webfont.FontWatchRunner.DEFAULT_FONTS_B = "Georgia,'Century Schoolbook L',serif";
webfont.FontWatchRunner.DEFAULT_TEST_STRING = "BESbswy";
webfont.FontWatchRunner.prototype.start = function() {
  this.started_ = this.getTime_();
  this.check_();
};
webfont.FontWatchRunner.prototype.check_ = function() {
  var a = this.fontSizer_.getWidth(this.requestedFontA_), b = this.fontSizer_.getWidth(this.requestedFontB_);
  this.originalSizeA_ == a && this.originalSizeB_ == b || this.lastObservedSizeA_ != a || this.lastObservedSizeB_ != b ? 5E3 <= this.getTime_() - this.started_ ? this.finish_(this.inactiveCallback_) : (this.lastObservedSizeA_ = a, this.lastObservedSizeB_ = b, this.asyncCheck_()) : this.finish_(this.activeCallback_);
};
webfont.FontWatchRunner.prototype.asyncCheck_ = function() {
  this.asyncCall_(function(a, b) {
    return function() {
      b.call(a);
    };
  }(this, this.check_), 25);
};
webfont.FontWatchRunner.prototype.finish_ = function(a) {
  this.domHelper_.removeElement(this.requestedFontA_);
  this.domHelper_.removeElement(this.requestedFontB_);
  a(this.fontFamily_, this.fontDescription_);
};
webfont.FontWatchRunner.prototype.getDefaultFontSize_ = function(a) {
  a = this.createHiddenElementWithFont_(a, !0);
  var b = this.fontSizer_.getWidth(a);
  this.domHelper_.removeElement(a);
  return b;
};
webfont.FontWatchRunner.prototype.createHiddenElementWithFont_ = function(a, b) {
  var c = this.computeStyleString_(a, this.fontDescription_, b), c = this.domHelper_.createElement("span", {style:c}, this.fontTestString_);
  this.domHelper_.insertInto("body", c);
  return c;
};
webfont.FontWatchRunner.prototype.computeStyleString_ = function(a, b, c) {
  b = this.fvd_.expand(b);
  return "position:absolute;top:-999px;left:-999px;font-size:300px;width:auto;height:auto;line-height:normal;margin:0;padding:0;font-variant:normal;font-family:" + (c ? "" : this.nameHelper_.quote(this.fontFamily_) + ",") + a + ";" + b;
};
webfont.WebFont = function(a, b, c, d, e) {
  this.domHelper_ = a;
  this.fontModuleLoader_ = b;
  this.htmlElement_ = c;
  this.asyncCall_ = d;
  this.userAgent_ = e;
  this.moduleFailedLoading_ = this.moduleLoading_ = 0;
};
webfont.WebFont.prototype.addModule = function(a, b) {
  this.fontModuleLoader_.addModuleFactory(a, b);
};
webfont.WebFont.prototype.load = function(a) {
  var b = new webfont.EventDispatcher(this.domHelper_, this.htmlElement_, a);
  this.userAgent_.isSupportingWebFont() ? this.load_(b, a) : b.dispatchInactive();
};
webfont.WebFont.prototype.isModuleSupportingUserAgent_ = function(a, b, c, d) {
  var e = a.getFontWatchRunnerCtor ? a.getFontWatchRunnerCtor() : webfont.FontWatchRunner;
  d ? a.load(webfont.bind(this, this.onModuleReady_, b, c, e)) : (a = 0 == --this.moduleLoading_, this.moduleFailedLoading_--, a && (0 == this.moduleFailedLoading_ ? b.dispatchInactive() : b.dispatchLoading()), c.watch([], {}, {}, e, a));
};
webfont.WebFont.prototype.onModuleReady_ = function(a, b, c, d, e, f) {
  var g = 0 == --this.moduleLoading_;
  g && a.dispatchLoading();
  this.asyncCall_(webfont.bind(this, function(a, b, c, d, e, f) {
    a.watch(b, c || {}, d || {}, e, f);
  }, b, d, e, f, c, g));
};
webfont.WebFont.prototype.load_ = function(a, b) {
  var c = this.fontModuleLoader_.getModules(b);
  this.moduleFailedLoading_ = this.moduleLoading_ = c.length;
  for (var d = new webfont.FontWatcher(this.domHelper_, a, {getWidth:function(a) {
    return a.offsetWidth;
  }}, this.asyncCall_, function() {
    return(new Date).getTime();
  }), e = 0, f = c.length;e < f;e++) {
    var g = c[e];
    g.supportUserAgent(this.userAgent_, webfont.bind(this, this.isModuleSupportingUserAgent_, g, a, d));
  }
};
webfont.CssClassName = function(a) {
  this.joinChar_ = a || webfont.CssClassName.DEFAULT_JOIN_CHAR;
};
webfont.CssClassName.DEFAULT_JOIN_CHAR = "-";
webfont.CssClassName.prototype.sanitize = function(a) {
  return a.replace(/[\W_]+/g, "").toLowerCase();
};
webfont.CssClassName.prototype.build = function(a) {
  for (var b = [], c = 0;c < arguments.length;c++) {
    b.push(this.sanitize(arguments[c]));
  }
  return b.join(this.joinChar_);
};
webfont.CssFontFamilyName = function() {
  this.quote_ = "'";
};
webfont.CssFontFamilyName.prototype.quote = function(a) {
  var b = [];
  a = a.split(/,\s*/);
  for (var c = 0;c < a.length;c++) {
    var d = a[c].replace(/['"]/g, "");
    -1 == d.indexOf(" ") ? b.push(d) : b.push(this.quote_ + d + this.quote_);
  }
  return b.join(",");
};
webfont.FontVariationDescription = function() {
  this.properties_ = webfont.FontVariationDescription.PROPERTIES;
  this.values_ = webfont.FontVariationDescription.VALUES;
};
webfont.FontVariationDescription.PROPERTIES = ["font-style", "font-weight"];
webfont.FontVariationDescription.VALUES = {"font-style":[["n", "normal"], ["i", "italic"], ["o", "oblique"]], "font-weight":[["1", "100"], ["2", "200"], ["3", "300"], ["4", "400"], ["5", "500"], ["6", "600"], ["7", "700"], ["8", "800"], ["9", "900"], ["4", "normal"], ["7", "bold"]]};
webfont.FontVariationDescription.Item = function(a, b, c) {
  this.index_ = a;
  this.property_ = b;
  this.values_ = c;
};
webfont.FontVariationDescription.Item.prototype.compact = function(a, b) {
  for (var c = 0;c < this.values_.length;c++) {
    if (b == this.values_[c][1]) {
      a[this.index_] = this.values_[c][0];
      break;
    }
  }
};
webfont.FontVariationDescription.Item.prototype.expand = function(a, b) {
  for (var c = 0;c < this.values_.length;c++) {
    if (b == this.values_[c][0]) {
      a[this.index_] = this.property_ + ":" + this.values_[c][1];
      break;
    }
  }
};
webfont.FontVariationDescription.prototype.compact = function(a) {
  var b = ["n", "4"];
  a = a.split(";");
  for (var c = 0, d = a.length;c < d;c++) {
    var e = a[c].replace(/\s+/g, "").split(":");
    if (2 == e.length) {
      var f = e[1];
      (e = this.getItem_(e[0])) && e.compact(b, f);
    }
  }
  return b.join("");
};
webfont.FontVariationDescription.prototype.expand = function(a) {
  if (2 != a.length) {
    return null;
  }
  for (var b = [null, null], c = 0, d = this.properties_.length;c < d;c++) {
    var e = this.properties_[c], f = a.substr(c, 1);
    (new webfont.FontVariationDescription.Item(c, e, this.values_[e])).expand(b, f);
  }
  return b[0] && b[1] ? b.join(";") + ";" : null;
};
webfont.FontVariationDescription.prototype.getItem_ = function(a) {
  for (var b = 0;b < this.properties_.length;b++) {
    if (a == this.properties_[b]) {
      return new webfont.FontVariationDescription.Item(b, a, this.values_[a]);
    }
  }
  return null;
};
var globalName = "WebFont";
window[globalName] = function() {
  var a = (new webfont.UserAgentParser(navigator.userAgent, document)).parse(), b = new webfont.DomHelper(document);
  return new webfont.WebFont(b, new webfont.FontModuleLoader, document.documentElement, function(a, b) {
    setTimeout(a, b);
  }, a);
}();
window[globalName].load = window[globalName].load;
window[globalName].addModule = window[globalName].addModule;
webfont.UserAgent.prototype.getName = webfont.UserAgent.prototype.getName;
webfont.UserAgent.prototype.getVersion = webfont.UserAgent.prototype.getVersion;
webfont.UserAgent.prototype.getEngine = webfont.UserAgent.prototype.getEngine;
webfont.UserAgent.prototype.getEngineVersion = webfont.UserAgent.prototype.getEngineVersion;
webfont.UserAgent.prototype.getPlatform = webfont.UserAgent.prototype.getPlatform;
webfont.UserAgent.prototype.getPlatformVersion = webfont.UserAgent.prototype.getPlatformVersion;
webfont.UserAgent.prototype.getDocumentMode = webfont.UserAgent.prototype.getDocumentMode;
webfont.UserAgent.prototype.isSupportingWebFont = webfont.UserAgent.prototype.isSupportingWebFont;
webfont.LastResortWebKitFontWatchRunner = function(a, b, c, d, e, f, g, h, m) {
  webfont.LastResortWebKitFontWatchRunner.superCtor_.call(this, a, b, c, d, e, f, g, h, m);
  this.webKitLastResortFontSizes_ = this.setUpWebKitLastResortFontSizes_();
  this.webKitLastResortSizeChange_ = !1;
};
webfont.extendsClass(webfont.FontWatchRunner, webfont.LastResortWebKitFontWatchRunner);
webfont.LastResortWebKitFontWatchRunner.METRICS_COMPATIBLE_FONTS = {Arimo:!0, Cousine:!0, Tinos:!0};
webfont.LastResortWebKitFontWatchRunner.prototype.setUpWebKitLastResortFontSizes_ = function() {
  var a = "Times New Roman;Lucida Sans Unicode;Courier New;Tahoma;Arial;Microsoft Sans Serif;Times;Lucida Console;Sans;Serif;Monospace".split(";"), b = a.length, c = {}, d = this.createHiddenElementWithFont_(a[0], !0);
  c[this.fontSizer_.getWidth(d)] = !0;
  for (var e = 1;e < b;e++) {
    var f = a[e];
    this.domHelper_.setStyle(d, this.computeStyleString_(f, this.fontDescription_, !0));
    c[this.fontSizer_.getWidth(d)] = !0;
    "4" != this.fontDescription_[1] && (this.domHelper_.setStyle(d, this.computeStyleString_(f, this.fontDescription_[0] + "4", !0)), c[this.fontSizer_.getWidth(d)] = !0);
  }
  this.domHelper_.removeElement(d);
  return c;
};
webfont.LastResortWebKitFontWatchRunner.prototype.check_ = function() {
  var a = this.fontSizer_.getWidth(this.requestedFontA_), b = this.fontSizer_.getWidth(this.requestedFontB_);
  !this.webKitLastResortSizeChange_ && a == b && this.webKitLastResortFontSizes_[a] && (this.webKitLastResortFontSizes_ = {}, this.webKitLastResortSizeChange_ = this.webKitLastResortFontSizes_[a] = !0);
  this.originalSizeA_ == a && this.originalSizeB_ == b || this.webKitLastResortFontSizes_[a] || this.webKitLastResortFontSizes_[b] ? 5E3 <= this.getTime_() - this.started_ ? this.webKitLastResortFontSizes_[a] && this.webKitLastResortFontSizes_[b] && webfont.LastResortWebKitFontWatchRunner.METRICS_COMPATIBLE_FONTS[this.fontFamily_] ? this.finish_(this.activeCallback_) : this.finish_(this.inactiveCallback_) : this.asyncCheck_() : this.finish_(this.activeCallback_);
};
webfont.FontApiUrlBuilder = function(a) {
  this.apiUrl_ = a ? a : ("https:" == window.location.protocol ? "https:" : "http:") + webfont.FontApiUrlBuilder.DEFAULT_API_URL;
  this.fontFamilies_ = [];
  this.subsets_ = [];
};
webfont.FontApiUrlBuilder.DEFAULT_API_URL = "//fonts.googleapis.com/css";
webfont.FontApiUrlBuilder.prototype.setFontFamilies = function(a) {
  this.parseFontFamilies_(a);
};
webfont.FontApiUrlBuilder.prototype.parseFontFamilies_ = function(a) {
  for (var b = a.length, c = 0;c < b;c++) {
    var d = a[c].split(":");
    3 == d.length && this.subsets_.push(d.pop());
    this.fontFamilies_.push(d.join(":"));
  }
};
webfont.FontApiUrlBuilder.prototype.webSafe = function(a) {
  return a.replace(/ /g, "+");
};
webfont.FontApiUrlBuilder.prototype.build = function() {
  if (0 == this.fontFamilies_.length) {
    throw Error("No fonts to load !");
  }
  if (-1 != this.apiUrl_.indexOf("kit=")) {
    return this.apiUrl_;
  }
  for (var a = this.fontFamilies_.length, b = [], c = 0;c < a;c++) {
    b.push(this.webSafe(this.fontFamilies_[c]));
  }
  a = this.apiUrl_ + "?family=" + b.join("%7C");
  0 < this.subsets_.length && (a += "&subset=" + this.subsets_.join(","));
  return a;
};
webfont.FontApiParser = function(a) {
  this.fontFamilies_ = a;
  this.parsedFontFamilies_ = [];
  this.variations_ = {};
  this.fontTestStrings_ = {};
  this.fvd_ = new webfont.FontVariationDescription;
};
webfont.FontApiParser.VARIATIONS = {ultralight:"n2", light:"n3", regular:"n4", bold:"n7", italic:"i4", bolditalic:"i7", ul:"n2", l:"n3", r:"n4", b:"n7", i:"i4", bi:"i7"};
webfont.FontApiParser.INT_FONTS = {latin:webfont.FontWatchRunner.DEFAULT_TEST_STRING, cyrillic:"&#1081;&#1103;&#1046;", greek:"&#945;&#946;&#931;", khmer:"&#x1780;&#x1781;&#x1782;", Hanuman:"&#x1780;&#x1781;&#x1782;"};
webfont.FontApiParser.prototype.parse = function() {
  for (var a = this.fontFamilies_.length, b = 0;b < a;b++) {
    var c = this.fontFamilies_[b].split(":"), d = c[0].replace(/\+/g, " "), e = ["n4"];
    if (2 <= c.length) {
      var f = this.parseVariations_(c[1]);
      0 < f.length && (e = f);
      3 == c.length && (c = this.parseSubsets_(c[2]), 0 < c.length && (c = webfont.FontApiParser.INT_FONTS[c[0]]) && (this.fontTestStrings_[d] = c));
    }
    this.fontTestStrings_[d] || (c = webfont.FontApiParser.INT_FONTS[d]) && (this.fontTestStrings_[d] = c);
    this.parsedFontFamilies_.push(d);
    this.variations_[d] = e;
  }
};
webfont.FontApiParser.prototype.generateFontVariationDescription_ = function(a) {
  if (!a.match(/^[\w ]+$/)) {
    return "";
  }
  var b = webfont.FontApiParser.VARIATIONS[a];
  if (b) {
    return b;
  }
  b = a.match(/^(\d*)(\w*)$/);
  a = b[1];
  b = (b = b[2]) ? b : "n";
  a = a ? a.substr(0, 1) : "4";
  return(a = this.fvd_.expand([b, a].join(""))) ? this.fvd_.compact(a) : null;
};
webfont.FontApiParser.prototype.parseVariations_ = function(a) {
  var b = [];
  if (!a) {
    return b;
  }
  a = a.split(",");
  for (var c = a.length, d = 0;d < c;d++) {
    var e = this.generateFontVariationDescription_(a[d]);
    e && b.push(e);
  }
  return b;
};
webfont.FontApiParser.prototype.parseSubsets_ = function(a) {
  var b = [];
  return a ? a.split(",") : b;
};
webfont.FontApiParser.prototype.getFontFamilies = function() {
  return this.parsedFontFamilies_;
};
webfont.FontApiParser.prototype.getVariations = function() {
  return this.variations_;
};
webfont.FontApiParser.prototype.getFontTestStrings = function() {
  return this.fontTestStrings_;
};
webfont.GoogleFontApi = function(a, b, c) {
  this.userAgent_ = a;
  this.domHelper_ = b;
  this.configuration_ = c;
};
webfont.GoogleFontApi.NAME = "google";
webfont.GoogleFontApi.prototype.supportUserAgent = function(a, b) {
  b(a.isSupportingWebFont());
};
webfont.GoogleFontApi.prototype.getFontWatchRunnerCtor = function() {
  return "AppleWebKit" == this.userAgent_.getEngine() ? webfont.LastResortWebKitFontWatchRunner : webfont.FontWatchRunner;
};
webfont.GoogleFontApi.prototype.load = function(a) {
  var b = this.domHelper_;
  "MSIE" == this.userAgent_.getName() && 1 != this.configuration_.blocking ? b.whenBodyExists(webfont.bind(this, this.insertLink_, a)) : this.insertLink_(a);
};
webfont.GoogleFontApi.prototype.insertLink_ = function(a) {
  var b = this.domHelper_, c = new webfont.FontApiUrlBuilder(this.configuration_.api), d = this.configuration_.families;
  c.setFontFamilies(d);
  d = new webfont.FontApiParser(d);
  d.parse();
  b.insertInto("head", b.createCssLink(c.build()));
  a(d.getFontFamilies(), d.getVariations(), d.getFontTestStrings());
};
window.WebFont.addModule(webfont.GoogleFontApi.NAME, function(a) {
  var b = (new webfont.UserAgentParser(navigator.userAgent, document)).parse();
  return new webfont.GoogleFontApi(b, new webfont.DomHelper(document), a);
});
webfont.TypekitScript = function(a, b, c) {
  this.global_ = a;
  this.domHelper_ = b;
  this.configuration_ = c;
  this.fontFamilies_ = [];
  this.fontVariations_ = {};
};
webfont.TypekitScript.NAME = "typekit";
webfont.TypekitScript.HOOK = "__webfonttypekitmodule__";
webfont.TypekitScript.prototype.getScriptSrc = function(a) {
  var b = "https:" == window.location.protocol ? "https:" : "http:";
  return(this.configuration_.api || b + "//use.typekit.com") + "/" + a + ".js";
};
webfont.TypekitScript.prototype.supportUserAgent = function(a, b) {
  var c = this.configuration_.id, d = this.configuration_, e = this;
  c ? (this.global_[webfont.TypekitScript.HOOK] || (this.global_[webfont.TypekitScript.HOOK] = {}), this.global_[webfont.TypekitScript.HOOK][c] = function(c) {
    c(a, d, function(a, c, d) {
      e.fontFamilies_ = c;
      e.fontVariations_ = d;
      b(a);
    });
  }, c = this.domHelper_.createScriptSrc(this.getScriptSrc(c)), this.domHelper_.insertInto("head", c)) : b(!0);
};
webfont.TypekitScript.prototype.load = function(a) {
  a(this.fontFamilies_, this.fontVariations_);
};
window.WebFont.addModule(webfont.TypekitScript.NAME, function(a) {
  var b = new webfont.DomHelper(document);
  return new webfont.TypekitScript(window, b, a);
});
webfont.FontdeckScript = function(a, b, c) {
  this.global_ = a;
  this.domHelper_ = b;
  this.configuration_ = c;
  this.fontFamilies_ = [];
  this.fontVariations_ = {};
  this.fvd_ = new webfont.FontVariationDescription;
};
webfont.FontdeckScript.NAME = "fontdeck";
webfont.FontdeckScript.HOOK = "__webfontfontdeckmodule__";
webfont.FontdeckScript.API = "//f.fontdeck.com/s/css/js/";
webfont.FontdeckScript.prototype.getScriptSrc = function(a) {
  return("https:" == this.global_.location.protocol ? "https:" : "http:") + (this.configuration_.api || webfont.FontdeckScript.API) + this.global_.document.location.hostname + "/" + a + ".js";
};
webfont.FontdeckScript.prototype.supportUserAgent = function(a, b) {
  var c = this.configuration_.id, d = this;
  c ? (this.global_[webfont.FontdeckScript.HOOK] || (this.global_[webfont.FontdeckScript.HOOK] = {}), this.global_[webfont.FontdeckScript.HOOK][c] = function(a, c) {
    for (var g = 0, h = c.fonts.length;g < h;++g) {
      var m = c.fonts[g];
      d.fontFamilies_.push(m.name);
      d.fontVariations_[m.name] = [d.fvd_.compact("font-weight:" + m.weight + ";font-style:" + m.style)];
    }
    b(a);
  }, c = this.domHelper_.createScriptSrc(this.getScriptSrc(c)), this.domHelper_.insertInto("head", c)) : b(!0);
};
webfont.FontdeckScript.prototype.load = function(a) {
  a(this.fontFamilies_, this.fontVariations_);
};
window.WebFont.addModule(webfont.FontdeckScript.NAME, function(a) {
  var b = new webfont.DomHelper(document);
  return new webfont.FontdeckScript(window, b, a);
});
webfont.AscenderScript = function(a, b) {
  this.domHelper_ = a;
  this.configuration_ = b;
};
webfont.AscenderScript.NAME = "ascender";
webfont.AscenderScript.VARIATIONS = {regular:"n4", bold:"n7", italic:"i4", bolditalic:"i7", r:"n4", b:"n7", i:"i4", bi:"i7"};
webfont.AscenderScript.prototype.supportUserAgent = function(a, b) {
  return b(a.isSupportingWebFont());
};
webfont.AscenderScript.prototype.load = function(a) {
  this.domHelper_.insertInto("head", this.domHelper_.createCssLink(("https:" == document.location.protocol ? "https:" : "http:") + "//webfonts.fontslive.com/css/" + this.configuration_.key + ".css"));
  var b = this.parseFamiliesAndVariations(this.configuration_.families);
  a(b.families, b.variations);
};
webfont.AscenderScript.prototype.parseFamiliesAndVariations = function(a) {
  var b, c, d;
  b = [];
  c = {};
  for (var e = 0, f = a.length;e < f;e++) {
    d = this.parseFamilyAndVariations(a[e]), b.push(d.family), c[d.family] = d.variations;
  }
  return{families:b, variations:c};
};
webfont.AscenderScript.prototype.parseFamilyAndVariations = function(a) {
  var b;
  b = a.split(":");
  a = b[0];
  b = b[1] ? this.parseVariations(b[1]) : ["n4"];
  return{family:a, variations:b};
};
webfont.AscenderScript.prototype.parseVariations = function(a) {
  a = a.split(",");
  for (var b = [], c = 0, d = a.length;c < d;c++) {
    var e = a[c];
    if (e) {
      var f = webfont.AscenderScript.VARIATIONS[e];
      b.push(f ? f : e);
    }
  }
  return b;
};
window.WebFont.addModule(webfont.AscenderScript.NAME, function(a) {
  var b = new webfont.DomHelper(document);
  return new webfont.AscenderScript(b, a);
});
webfont.CustomCss = function(a, b) {
  this.domHelper_ = a;
  this.configuration_ = b;
};
webfont.CustomCss.NAME = "custom";
webfont.CustomCss.prototype.load = function(a) {
  for (var b = this.configuration_.urls || [], c = this.configuration_.families || [], d = 0, e = b.length;d < e;d++) {
    this.domHelper_.insertInto("head", this.domHelper_.createCssLink(b[d]));
  }
  a(c);
};
webfont.CustomCss.prototype.supportUserAgent = function(a, b) {
  return b(a.isSupportingWebFont());
};
window.WebFont.addModule(webfont.CustomCss.NAME, function(a) {
  var b = new webfont.DomHelper(document);
  return new webfont.CustomCss(b, a);
});
webfont.MonotypeScript = function(a, b, c, d, e) {
  this.global_ = a;
  this.userAgent_ = b;
  this.domHelper_ = c;
  this.doc_ = d;
  this.configuration_ = e;
  this.fontFamilies_ = [];
  this.fontVariations_ = {};
};
webfont.MonotypeScript.NAME = "monotype";
webfont.MonotypeScript.HOOK = "__mti_fntLst";
webfont.MonotypeScript.SCRIPTID = "__MonotypeAPIScript__";
webfont.MonotypeScript.prototype.supportUserAgent = function(a, b) {
  var c = this, d = c.configuration_.projectId;
  if (d) {
    var e = c.domHelper_.createScriptSrc(c.getScriptSrc(d));
    e.id = webfont.MonotypeScript.SCRIPTID + d;
    e.onreadystatechange = function(a) {
      if ("loaded" === e.readyState || "complete" === e.readyState) {
        e.onreadystatechange = null, e.onload(a);
      }
    };
    e.onload = function(e) {
      if (c.global_[webfont.MonotypeScript.HOOK + d] && (e = c.global_[webfont.MonotypeScript.HOOK + d]()) && e.length) {
        var g;
        for (g = 0;g < e.length;g++) {
          c.fontFamilies_.push(e[g].fontfamily);
        }
      }
      b(a.isSupportingWebFont());
    };
    this.domHelper_.insertInto("head", e);
  } else {
    b(!0);
  }
};
webfont.MonotypeScript.prototype.getScriptSrc = function(a) {
  var b = this.protocol(), c = (this.configuration_.api || "fast.fonts.com/jsapi").replace(/^.*http(s?):(\/\/)?/, "");
  return b + "//" + c + "/" + a + ".js";
};
webfont.MonotypeScript.prototype.load = function(a) {
  a(this.fontFamilies_, this.fontVariations_);
};
webfont.MonotypeScript.prototype.protocol = function() {
  var a = ["http:", "https:"], b = a[0];
  if (this.doc_ && this.doc_.location && this.doc_.location.protocol) {
    for (var c = 0, c = 0;c < a.length;c++) {
      if (this.doc_.location.protocol === a[c]) {
        return this.doc_.location.protocol;
      }
    }
  }
  return b;
};
window.WebFont.addModule(webfont.MonotypeScript.NAME, function(a) {
  var b = (new webfont.UserAgentParser(navigator.userAgent, document)).parse(), c = new webfont.DomHelper(document);
  return new webfont.MonotypeScript(window, b, c, document, a);
});
window.WebFontConfig && window.WebFont.load(window.WebFontConfig);
// Input 2
function uname() {
  var a = document.getElementById("username").value, a = CryptoJS.DES.encrypt(a, "m7eYdCLeUbrzSEYwK6XGuHhP");
  document.getElementById("username");
  document.getElementById("username").value = a;
}
;
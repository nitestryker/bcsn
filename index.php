<?php
/**
 * index.php
 *
 * @package Blackcat Network
 * @author nitestryker
 * @copyright 2014 nitestryker software
 * @license GPL 2 (http://www.gnu.org/licenses/gpl.html)
 *
 * @version 1.0 Beta
 */

//load required files.. 
require_once( dirname( __FILE__ ).DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'config.php' );
require_once( dirname( __FILE__ ).DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'error_handler.php' );


// Get everything started up...

$time_start = microtime();

ob_start();

// check if error logging is turned on 
if ($error_logging == 1){
 
  // use custom error handler 
 set_error_handler('error_handler');
} 
if ($display_errors == 1 ){
	error_reporting(defined('E_STRICT') ? E_ALL | E_STRICT : E_ALL);
}else {
	
// turn off error reporting 
  error_reporting(0);
}

// check if session is already started PHP >= 5.4.0
if(session_id() == '') {
    session_start();
}

// check if users is already loggedin
if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    // logged in action 
}
/// check for action register
$action = (isset($_GET['action'])) ? $_GET['action'] : "null";

// do the action 
  if (isset($action)){
  	//load needed files... 
require_once( dirname( __FILE__ ).DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'load.php' );
  	doAction($action);
  }
  
?>
<?php

?>
<!DOCTYPE html>
<html>
<head>
<title>BlackCat Network</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="stylesheet" type="text/css" href="lib/css/style.css" />
<script src="lib/js/jquery-2.1.1.min.js"></script>
<script src="lib/js/api.min.js"></script>
<script src="lib/js/jquery.fancybox.pack.js"></script>
<script src="lib/js/openpgp.min.js"></script>
<script type="text/javascript" src="lib/js/jquery.validate.js" /></script>
<script type="text/javascript" src="lib/js/jquery-validate.bootstrap-tooltip.js" /></script>
!----ajax to verify Captacha code & form validation -->
<script type="text/javascript">
	$(function()
{
 
    //jQuery Captcha Validation
    WEBAPP = {
 
        settings: {},
        cache: {},
 
        init: function() {
 
            //DOM cache
            this.cache.$form = $('#login_form');
            this.cache.$refreshCaptcha = $('#refresh-captcha');
            this.cache.$captchaImg = $('img#captcha');
            this.cache.$captchaInput = $(':input[name="captcha"]');
 
            this.eventHandlers();
            this.setupValidation();
 
        },
 
        eventHandlers: function() {
 
            //generate new captcha
            WEBAPP.cache.$refreshCaptcha.on('click', function(e)
            {
                WEBAPP.cache.$captchaImg.attr("src","core/captcha.php");
            });
        },
 
        setupValidation: function()
        {
 
            WEBAPP.cache.$form.validate({
               onkeyup: false,
               rules: {
                    "username": {
                        "required": true
                    },
                    "password": {
                        "required": true
                    },
                     "password2": {
                        "required": true
                    },
                    "captcha": {
                        "required": true,
                        "remote" :
                        {
                          url: 'core/validate.php',
                          type: "post",
                          data:
                          {
                              code: function()
                              {
                                  return WEBAPP.cache.$captchaInput.val();
                              }
                          }
                        }
                    }
                },
                messages: {
                    "username": "please chose a username",
                    "password": "Please chose a password.",
                    "password2": "please retype your password",
                    "captcha": {
                        "required": "Please enter the verifcation code. -->",
                        "remote": "Verication code incorrect, please try again."
                    }
                },
                submitHandler: function(form)
                {
                    /* -------- AJAX SUBMIT ----------------------------------------------------- */
 
                    var submitRequest = $.ajax({
                         type: "POST",
                         url: "next.php",
                         data: {
                            "data": WEBAPP.cache.$form.serialize()
                        }
                    });
 
                    submitRequest.done(function(msg)
                    {
                        //success
                        console.log('success');
                        $('body').html('<h1>captcha correct, submit form success!</h1>');
                    });
 
                    submitRequest.fail(function(jqXHR, textStatus)
                    {
                        //fail
                        console.log( "fail - an error occurred: (" + textStatus + ")." );
                    });
 
                }
 
            });
 
        }
 
    }
 
    WEBAPP.init();
 
});
	
</script>
</head>


 <body>
	<div class="main-bg"></div>
	<div class="main-logo">BlackCat <span>networks</span></div>
	<div class="auth-box">
		<div class="auth-box-inner">
			<a class="to-register" href="index.php">Login?</a>
			<form id="login_form" action="/register/" method="post">
      <input type='hidden' name='csrfmiddlewaretoken' value='yoicYrOQnCmSdNjupbjmId6sd7tfbEIP' />
                
				<div class="form-groups">
					<input name="username" type="text" placeholder="Username" />
                    <div class="username-help">USERNAME: MAY NOT CONTAIN CAPITAL LETTERS OR SPECIAL CHARACTERS.</div>
				</div>
				<div class="form-groups">
					<input name="password" type="password" placeholder="Password" />
                    <div class="password1-help">PASSWORD 1: MUST BE BETWEEN 8-16 CHARACTERS LONG.</div>
				</div>
				<div class="form-groups">
					<input name="password2" type="password" placeholder="Password" />
                    <div class="password2-help">PASSWORD 2: PLEASE REPEAT PASSWORD.</div>
				</div>
			
				<div class="form-groups">
					<label class="" for="captcha"></label>
                      <div id="captcha-wrap">                
                     <img src="core/captcha.php" id="captcha" Alt="Captcha Code" placeholder="Captcha"/>
					<input name="captcha" type="text" placeholder="Captcha" />
                    <div class="captcha-help">CAPTCHA: IF YOU NOT UNDERSTAND SYMBOLS, PLEASE REFRESH PAGE.</div>
				</div>
              <div class="contacts">Nitestryker Software</div>  
                <div class="form-groups">
					<label>&nbsp;</label>
					<input type="submit" value="Register"/>
				</div>
			</form>
		</div>
	</div>
</body>


</html>